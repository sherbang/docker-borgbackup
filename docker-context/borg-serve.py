#!/usr/bin/env python3

import os
import sys

import psutil

if len(sys.argv) != 2:
    print(f"Usage: {sys.argv[0]} <hostname>")
    sys.exit(1)

hostname = sys.argv[1]

if hostname not in ["slave", "stick", "earth", "bethselamin", "vogsphere"]:
    raise Exception("Invalid hostname")  # noqa: TRY002, TRY003

borg_repo = f"~/borgbackup/{hostname}.borg"
borg_serve_command = f"/usr/bin/borg serve --restrict-to-repository {borg_repo}"
borg_compact_command = f"/usr/bin/borg compact {borg_repo}"

if os.environ["SSH_ORIGINAL_COMMAND"].startswith("/usr/bin/borg serve"):
    # Command input has been sufficiently sanitized above
    os.system(borg_serve_command)  # noqa: S605
    os.system(borg_compact_command)  # noqa: S605
elif os.environ["SSH_ORIGINAL_COMMAND"] == "shutdown":
    borg_running = False
    for proc in psutil.process_iter():
        if proc.name() == "borg":
            borg_running = True

    if borg_running:
        print("Found running borg process")
    else:
        os.system("(sleep 1 && sudo kill 1) & exit")  # noqa: S605, S607
else:
    print("Invalid command: {}".format(os.environ["SSH_ORIGINAL_COMMAND"]))
    sys.exit(1)
